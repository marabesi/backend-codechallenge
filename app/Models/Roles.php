<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{

    public static $roles = [
        1 => 'Client',
        2 => 'Driver'
    ];
}
