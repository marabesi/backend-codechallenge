<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = [
        'name',
        'address',
        'delivery_date',
        'time_range_start',
        'time_range_end',
    ];
}
