<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Artisan;

class UserResourceTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();
        Artisan::call('db:seed');
    }

    public function userDataProvider()
    {
        return [
            'Request with all fields fine' => [
                [
                    'name' => 'test user',
                    'email' => 'test@test.com',
                    'password' => '123456',
                    'role_id' => 1,
                ],
                'result' => 'User have been created',
            ],
            'Missing name field' => [
                [
                    'email' => 'test@test.com',
                    'password' => '123456',
                ],
                'result' => 'The name field is required.',
            ],
            'Missing email field' => [
                [
                    'name' => 'my name',
                    'password' => '123456',
                ],
                'result' => 'The email field is required.',
            ],
            'Missing password field' => [
                [
                    'name' => 'my name',
                    'email' => 'testing@testing.com',
                ],
                'result' => 'The password field is required.',
            ],
            'Invalid email' => [
                [
                    'name' => 'my name',
                    'password' => '123456',
                    'email' => 'testing',
                ],
                'result' => 'The email must be a valid email address',
            ],
        ];
    }

    /**
     * @dataProvider userDataProvider
     */
    public function testShouldCreateAuser($userData, $result)
    {
        $this->json('POST', '/api/user', $userData)
            ->assertSee($result);
    }

    public function testShouldNotAllowUserWithDuplicatedEmail()
    {
        $data = [
            'name' => 'test1 user',
            'email' => 'test1@test.com',
            'password' => '12345678',
            'role_id' => 1,
        ];

        $this->json('POST', '/api/user', $data)
            ->assertJson([
                'User have been created'
            ]);

        $this->json('POST', '/api/user', $data)
            ->assertSee(
                'The email has already been taken.'
            );
    }
}
