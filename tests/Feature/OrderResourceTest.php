<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Artisan;

class OrderResourceTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();
        Artisan::call('db:seed');
    }

    public function orderDataProvider()
    {
        return [
            'Request with all fields fine' => [
                [
                    'name' => 'order n 1',
                    'address' => 'Calle test 20',
                    'delivery_date' => '2018-01-01',
                    'time_range_start' => 1,
                    'time_range_end' => 1,
                ],
                'result' => 'Order created'
            ],
            'Missing name field' => [
                [
                    'address' => 'Calle test 20',
                    'delivery_date' => '2018-01-01',
                    'time_range_start' => 1,
                    'time_range_end' => 1,
                ],
                'result' => 'The name field is required.'
            ],
            'Missing address field' => [
                [
                    'name' => 'order n 1',
                    'delivery_date' => '2018-01-01',
                    'time_range_start' => 1,
                    'time_range_end' => 1,
                ],
                'result' => 'The address field is required.'
            ],
            'Missing delivery_date field' => [
                [
                    'name' => 'order n 1',
                    'address' => 'Calle test 20',
                    'time_range_start' => 1,
                    'time_range_end' => 1,
                ],
                'result' => 'The delivery date field is required.'
            ]
        ];
    }

    /**
     * @dataProvider orderDataProvider
     */
    public function testShouldCreateAorder($orderData, $result)
    {
        $response = $this->json('POST', '/api/order', $orderData);

        $response->assertSee($result);
    }
}
