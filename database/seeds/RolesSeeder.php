<?php

use Illuminate\Database\Seeder;

use App\Models\Roles;

class RolesSeeder extends Seeder
{

    public function run()
    {
        foreach (Roles::$roles as $id => $description) {
            Roles::updateOrCreate([
                'id' => $id,
                'description' => $description,
            ]);
        }
    }
}
