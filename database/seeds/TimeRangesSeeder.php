<?php

use Illuminate\Database\Seeder;
use App\Models\TimeRanges;

class TimeRangesSeeder extends Seeder
{

    public function run()
    {
        foreach (TimeRanges::$ranges as $id => $description) {
            TimeRanges::updateOrCreate([
                'id' => $id,
                'description' => $description,
            ]);
        }
    }
}
