<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeRangeToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedInteger('time_range_start');
            $table->unsignedInteger('time_range_end');

            $table->foreign('time_range_start')
                ->references('id')->on('time_ranges');

            $table->foreign('time_range_end')
                ->references('id')->on('time_ranges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('orders_time_range_start_foreign');
            $table->dropForeign('orders_time_range_end_foreign');

            $table->dropColumn('time_range_start');
            $table->dropColumn('time_range_end');
        });
    }
}
